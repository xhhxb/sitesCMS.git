package com.sites.api;

import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Page;
import com.sites.common.model.Article;
import com.sites.common.model.Column;

import java.util.List;

/**
 * 数据接口控制器
 *
 * @author zyg
 * 2022年6月13日 下午15:22:02
 */
public class ApiController extends Controller {

    @Inject
    private ApiService srv;

    /**
     * 获取指定栏目文章列表，支持分页
     */
    public void getColumnArts(){
        int page = getParaToInt("page", 1);
        int limit = getParaToInt("limit", 8);
        String colEnName = getPara("col");
        Column column = srv.getColumnByEnName(colEnName);
        Page<Article> artPage = srv.queryArtsPage(page, limit, column.getId());

        Ret ret = Ret.ok();
        ret.set("colEnName", colEnName);
        ret.set("artPage", artPage);
        ret.set("column", column);
        renderJson(ret);
    }

    /**
     * 获取指定栏目(支持多个)文章，支持分页
     */
    public void getColsArts(){
        int page = getParaToInt("page", 1);
        int limit = getParaToInt("limit", 8);
        String cols = getPara("cols");
        Page<Article> artPage = srv.queryArtsPagePro(page, limit, cols);

        Ret ret = Ret.ok();
        ret.set("artPage", artPage);
        renderJson(ret);
    }

    /**
     * 根据文章id获取文章内容
     */
    public void getArtById(){
        int id = getInt("id");
        Article article = srv.getArtById(id);
        Ret ret = Ret.ok();
        ret.set("art", article);
        renderJson(ret);
    }

    /**
     * 获取一级栏目下的子栏目及子栏目文章列表
     */
    public void getColsAndArts(){
        int orderType = getInt("orderType");
        String colEnName = getPara("col");
        Column column = srv.getColumnByEnName(colEnName);
        List<Column> columnList = srv.getColsAndArts(column.getId(), orderType);
        Ret ret = Ret.ok();
        ret.set("columnList", columnList);
        renderJson(ret);
    }

}
