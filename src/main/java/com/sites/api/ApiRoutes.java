package com.sites.api;

import com.jfinal.config.Routes;

/**
 * api路由配置
 *
 * @author zyg
 * 2022年6月13日 下午15:22:02
 */
public class ApiRoutes extends Routes {

    @Override
    public void config() {
        addInterceptor(new ApiInterceptor());
        add("/api", ApiController.class);
    }

}
