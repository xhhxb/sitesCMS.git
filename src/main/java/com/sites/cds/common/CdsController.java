package com.sites.cds.common;

import com.jfinal.aop.Before;
import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Page;
import com.sites.common.RewriteInterceptor;
import com.sites.common.model.Article;
import com.sites.common.model.Column;

/**
 * cds公用控制器
 * 
 * @author zyg
 * 2020年2月8日 下午3:58:18
 */
public class CdsController extends Controller {

	@Inject
	private CdsService srv;
	
	/**
	 * 首页
	 */
	@Before(RewriteInterceptor.class)
	public void index(){
		render("index.html");
	}
	
	/**
	 * 二级页面：普通栏目文章列表页面
	 */
	public void enterColArts(){
		int page = getParaToInt("page", 1);
		int limit = getParaToInt("limit", 8);
		String colEnName = getPara("col");
		Column column = srv.getColumnByEnName(colEnName);
		Column parentColumn = srv.getColumnByChild(column);
		Page<Article> artPage = srv.queryArtsPage(page, limit, column.getId());
		setAttr("colEnName", colEnName);
		setAttr("artPage", artPage);
		setAttr("column", column);
		setAttr("parentColumn", parentColumn);
		keepPara();
		render("pageColArts.html");
	}
	
	/**
	 * 二级页面：图片形式的栏目文章列表页面
	 */
	public void enterColPicArts(){
		int page = getParaToInt("page", 1);
		int limit = getParaToInt("limit", 8);
		String colEnName = getPara("col");
		Column column = srv.getColumnByEnName(colEnName);
		Column parentColumn = srv.getColumnByChild(column);
		Page<Article> artPage = srv.queryArtsPage(page, limit, column.getId());
		setAttr("colEnName", colEnName);
		setAttr("artPage", artPage);
		setAttr("column", column);
		setAttr("parentColumn", parentColumn);
		keepPara();
		render("pageColPicArts.html");
	}
	
	/**
	 * 二级页面：视频栏目文章列表页面
	 */
	public void enterColVideoArts(){
		int page = getParaToInt("page", 1);
		int limit = getParaToInt("limit", 8);
		String colEnName = getPara("col");
		Column column = srv.getColumnByEnName(colEnName);
		Column parentColumn = srv.getColumnByChild(column);
		Page<Article> artPage = srv.queryArtsPage(page, limit, column.getId());
		setAttr("colEnName", colEnName);
		setAttr("artPage", artPage);
		setAttr("column", column);
		setAttr("parentColumn", parentColumn);
		keepPara();
		render("pageColVideoArts.html");
	}
	
	/**
	 * 二级页面：特殊形式的栏目文章列表页面
	 */
	public void enterColSpecialArts(){
		int page = getParaToInt("page", 1);
		int limit = getParaToInt("limit", 8);
		String colEnName = getPara("col");
		Column column = srv.getColumnByEnName(colEnName);
		Column parentColumn = srv.getColumnByChild(column);
		Page<Article> artPage = srv.queryArtsPage(page, limit, column.getId());
		setAttr("colEnName", colEnName);
		setAttr("artPage", artPage);
		setAttr("column", column);
		setAttr("parentColumn", parentColumn);
		keepPara();
		render("pageColSpecialArts.html");
	}
	
	/**
	 * 二级页面：其他形式的栏目文章列表页面
	 */
	public void enterColOtherArts(){
		int page = getParaToInt("page", 1);
		int limit = getParaToInt("limit", 8);
		String colEnName = getPara("col");
		String viewName = getPara("view", "pageColOtherArts");
		Column column = srv.getColumnByEnName(colEnName);
		Column parentColumn = srv.getColumnByChild(column);
		Page<Article> artPage = srv.queryArtsPage(page, limit, column.getId());
		setAttr("artPage", artPage);
		setAttr("column", column);
		setAttr("parentColumn", parentColumn);
		keepPara();
		viewName = viewName + ".html";
		render(viewName);
	}
	
	/**
	 * 三级页面：普通文章页面
	 */
	public void viewArt(){
		int id = getParaToInt("id");
		Article article = srv.getArtById(id);
		srv.clickArt(id);
		Column column = srv.getColumnById(article.getColumn());
		Column parentColumn = srv.getColumnByChild(column);
		setAttr("article", article);
		setAttr("column", column);
		setAttr("parentColumn", parentColumn);
		setAttr("colEnName", column.getEnName());
		render("pageArt.html");
	}
	
	/**
	 * 三级页面：图片文章页面
	 */
	public void viewPicArt(){
		int id = getParaToInt("id");
		Article article = srv.getArtById(id);
		srv.clickArt(id);
		Column column = srv.getColumnById(article.getColumn());
		Column parentColumn = srv.getColumnByChild(column);
		setAttr("article", article);
		setAttr("column", column);
		setAttr("parentColumn", parentColumn);
		setAttr("colEnName", column.getEnName());
		render("pagePicArt.html");
	}
	
	/**
	 * 三级页面：视频文章页面
	 */
	public void viewVideoArt(){
		int id = getParaToInt("id");
		Article article = srv.getArtById(id);
		srv.clickArt(id);
		Column column = srv.getColumnById(article.getColumn());
		Column parentColumn = srv.getColumnByChild(column);
		setAttr("article", article);
		setAttr("column", column);
		setAttr("parentColumn", parentColumn);
		setAttr("colEnName", column.getEnName());
		render("pageVideoArt.html");
	}
	
	/**
	 * 三级页面：特殊文章页面
	 */
	public void viewSpecialArt(){
		int id = getParaToInt("id");
		Article article = srv.getArtById(id);
		srv.clickArt(id);
		Column column = srv.getColumnById(article.getColumn());
		Column parentColumn = srv.getColumnByChild(column);
		setAttr("article", article);
		setAttr("column", column);
		setAttr("parentColumn", parentColumn);
		setAttr("colEnName", column.getEnName());
		render("pageSpecialArt.html");
	}
	
	/**
	 * 三级页面：其他形式的文章页面
	 */
	public void viewOtherArt(){
		int id = getParaToInt("id");
		Article article = srv.getArtById(id);
		srv.clickArt(id);
		Column column = srv.getColumnById(article.getColumn());
		Column parentColumn = srv.getColumnByChild(column);
		setAttr("article", article);
		setAttr("column", column);
		setAttr("parentColumn", parentColumn);
		setAttr("colEnName", column.getEnName());
		
		String viewName = getPara("view", "pageOtherArt");
		viewName = viewName + ".html";
		render(viewName);
	}
	
	/**
	 * 查看指定栏目最新的一篇文章
	 * 重点是最新的一篇，只查询一篇，可以用来展示联系我们等这种文章
	 * 
	 * 按先后顺序有两个参数：第一个是栏目英文名，第二个是用来展示的界面名称
	 */
	public void viewColLastArt(){
		//按先后顺序有两个参数：第一个是栏目英文名，第二个是用来展示的界面名称
		String colEnName = getPara(0);
		String viewName = get(1);
		viewName = viewName + ".html";
		Column column = srv.getColumnByEnName(colEnName);
		Article article = srv.getColLastArt(column.getId());
		srv.clickArt(article.getId());
		setAttr("article", article);
		setAttr("column", column);
		setAttr("colEnName", column.getEnName());
		
		render(viewName);
	}
	
	/**
	 * 直接跳转到界面
	 * 
	 * 只有一个参数：界面名称
	 */
	public void viewPage(){
		String viewName = get(0);
		viewName = viewName + ".html";
		render(viewName);
	}
	
	/**
	 * 根据文章id获取文章内容
	 */
	public void getArtContent() {
		int id = getParaToInt(0);
		Article article = srv.getArtById(id);
		renderText(article.getContent());
	}

	/**
	 * 手册/文档专用方法
	 */
	public void doc(){
		int id = getInt(0);
		Article article = srv.getArtById(id);
		set("doc", article);

		srv.clickArt(id);//增加文章点击次数

		boolean isPjax = "true".equalsIgnoreCase(getHeader("X-PJAX"));
		set("isPjax", isPjax);//页面根据这个参数来判断进行局部页面刷新还是全部页面渲染

		render("doc.html");
	}
	
}
