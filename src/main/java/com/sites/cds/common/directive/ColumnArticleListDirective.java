package com.sites.cds.common.directive;

import java.util.List;

import com.jfinal.aop.Aop;
import com.jfinal.template.Directive;
import com.jfinal.template.Env;
import com.jfinal.template.TemplateException;
import com.jfinal.template.expr.ast.Expr;
import com.jfinal.template.expr.ast.ExprList;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.ParseException;
import com.jfinal.template.stat.Scope;
import com.sites.common.model.Article;

/**
 * 栏目文章指令(置顶的默认放在最前面)
 * 
 * 使用方法一：栏目标识(暂时只支持栏目英文名获取数据)
 * #cald(columnName, listSize, paraName)
 * 		<li><a href="#(paraName.getArticleUrl())">#(paraName.title)</a></li>
 * #end
 * 
 * 使用方法二：不指定数据的参数名，默认使用a
 * #cald(columnName, listSize)
 * 		<li><a href="#(a.getArticleUrl())">#(a.title)</a></li>
 * #end
 * 
 * @author zyg
 * 2020年1月3日 下午8:58:16
 */
public class ColumnArticleListDirective extends Directive {
	
	private Expr columnNameExpr;//指令的第一个参数：栏目英文名
	private Expr listSizeExpr;//指令的第二个参数：文章列表数
	private Expr paraNameExpr;//指令的第三个参数：放置数据的参数名，可选
	private String columnName;//栏目名称标识
	private int listSize;//需要显示的文章数
	private String paraName = "a";//放置数据的参数名，有默认值
	private int paraNums;//指令参数个数
	
	private DirectiveService srv = Aop.get(DirectiveService.class);
	
	@Override
	public void setExprList(ExprList exprList) {
		paraNums = exprList.length();
		//指令参数个数校验
		if(paraNums<2 || paraNums>3){
			throw new ParseException("Wrong number parameter of #columnArticleList directive, two or three parameters allowed", location);
		}
		columnNameExpr = exprList.getExpr(0);
		listSizeExpr = exprList.getExpr(1);
		if(paraNums==3){
			paraNameExpr = exprList.getExpr(2);
		}
	}
	

	@Override
	public void exec(Env env, Scope scope, Writer writer) {
		Object columnNameObject = columnNameExpr.eval(scope);
		if(columnNameObject instanceof String){
			columnName = (String) columnNameObject;
		} else {
			throw new TemplateException("The first parameter of #columnArticleList directive must be String type", location);
		}
		
		Object listSizeObject = listSizeExpr.eval(scope);
		if(listSizeObject instanceof Integer){
			listSize = (Integer) listSizeObject;
		} else {
			throw new TemplateException("The second parameter of #columnArticleList directive must be Integer type", location);
		}
		
		if(paraNums==3){
			Object paraNameObject = paraNameExpr.eval(scope);
			if(paraNameObject instanceof String){
				paraName = (String)paraNameObject;
			} else {
				throw new TemplateException("The third parameter of #columnArticleList directive must be String type", location);
			}
		}
		
		//数据库查询文章列表，使用缓存
		List<Article> articles = srv.getArtListByCol(columnName, listSize);
		for(Article article : articles){
			scope.set(paraName, article);
			stat.exec(env, scope, writer);
		}
	}

	//有结束标签
	@Override
	public boolean hasEnd() {
		return true;
	}
	
}
