package com.sites.cds.common.directive;

import java.util.List;

import com.jfinal.aop.Aop;
import com.jfinal.template.Directive;
import com.jfinal.template.Env;
import com.jfinal.template.TemplateException;
import com.jfinal.template.expr.ast.Expr;
import com.jfinal.template.expr.ast.ExprList;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.ParseException;
import com.jfinal.template.stat.Scope;
import com.sites.common.model.Article;

/**
 * 升级版的 栏目文章指令(置顶的默认放在最前面)
 * 
 * 使用方法：栏目标识(暂时只支持栏目英文名获取数据)
 * #cald(columnName, listSize, paraName, orderType)
 * 		<li><a href="#(paraName.getArticleUrl())">#(paraName.title)</a></li>
 * #end
 * 
 * @author zyg
 * 2021年3月23日 下午10:03:10
 */
public class ColumnArticleListProDirective extends Directive {
	
	private Expr columnNameExpr;//指令的第一个参数：栏目英文名
	private Expr listSizeExpr;//指令的第二个参数：文章列表数
	private Expr paraNameExpr;//指令的第三个参数：放置数据的参数名
	private Expr orderTypeExpr;//指令的第四个参数：数据查询排序类型
	private String columnName;//栏目名称标识
	private int listSize;//需要显示的文章数
	private String paraName;//放置数据的参数名，升级版的参数固定，不需要默认值了
	private int orderType;//数据查询的排序方式：1按创建时间倒叙，2按创建时间正序，3按标题正序，4按标题倒叙。注意所有的排序都是基于置顶在最前面的基础上
	private int paraNums;//指令参数个数
	
	private DirectiveService srv = Aop.get(DirectiveService.class);
	
	@Override
	public void setExprList(ExprList exprList) {
		paraNums = exprList.length();
		//指令参数个数校验
		if(paraNums != 4){
			throw new ParseException("Wrong number parameter of #ColumnArticleListPro directive, four parameters allowed", location);
		}
		columnNameExpr = exprList.getExpr(0);
		listSizeExpr = exprList.getExpr(1);
		paraNameExpr = exprList.getExpr(2);
		orderTypeExpr = exprList.getExpr(3);
	}
	

	@Override
	public void exec(Env env, Scope scope, Writer writer) {
		Object columnNameObject = columnNameExpr.eval(scope);
		if(columnNameObject instanceof String){
			columnName = (String) columnNameObject;
		} else {
			throw new TemplateException("The first parameter of #ColumnArticleListPro directive must be String type", location);
		}
		
		Object listSizeObject = listSizeExpr.eval(scope);
		if(listSizeObject instanceof Integer){
			listSize = (Integer) listSizeObject;
		} else {
			throw new TemplateException("The second parameter of #ColumnArticleListPro directive must be Integer type", location);
		}
		
		Object paraNameObject = paraNameExpr.eval(scope);
		if(paraNameObject instanceof String){
			paraName = (String)paraNameObject;
		} else {
			throw new TemplateException("The third parameter of #ColumnArticleListPro directive must be String type", location);
		}
		
		Object orderTypeObject = orderTypeExpr.eval(scope);
		if(orderTypeObject instanceof Integer){
			orderType = (Integer) orderTypeObject;
		} else {
			throw new TemplateException("The fourth parameter of #ColumnArticleListPro directive must be Integer type", location);
		}
		
		//数据库查询文章列表，使用缓存
		List<Article> articles = srv.getArtListByCol(columnName, listSize, orderType);
		for(Article article : articles){
			scope.set(paraName, article);
			stat.exec(env, scope, writer);
		}
	}

	//有结束标签
	@Override
	public boolean hasEnd() {
		return true;
	}
	
}
