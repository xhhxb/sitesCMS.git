package com.sites.cds.config;

import com.jfinal.config.Routes;
import com.sites.cds.common.CdsAccessInterceptor;
import com.sites.cds.common.CdsController;
import com.sites.cds.common.ViewPathInterceptor;

/**
 * cds路由配置
 * 
 * @author zyg
 * 2020年1月2日 下午10:16:02
 */
public class CdsRoutes extends Routes {

	@Override
	public void config() {
		setBaseViewPath("/cds");// 配置页面基准路径
		
		addInterceptor(new ViewPathInterceptor());// 添加视图拦截器，主要用于区分多站点
		addInterceptor(new CdsAccessInterceptor());// 添加访问日志拦截器，记录访问信息
		
		add("/", CdsController.class);
	}

}
