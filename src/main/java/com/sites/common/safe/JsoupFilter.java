package com.sites.common.safe;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;

import com.jfinal.plugin.activerecord.Model;
import com.sites.common.model.Article;
import com.sites.common.model.Site;

/**
 * 使用 Jsoup 对 html 进行过滤
 * <br>参考JFinal-club方案 https://jfinal.com/club
 * 
 * @author zyg
 * 2021年2月2日 下午9:41:15
 */
public class JsoupFilter {

	/**
	 * 用于过滤 content 字段的白名单，需要允许比较多的 tag
	 */
	private static final Whitelist contentWhitelist = createContentWhitelist();
	private static final Document.OutputSettings notPrettyPrint = new Document.OutputSettings().prettyPrint(false);

	private static Whitelist createContentWhitelist() {
		/**
		 * 富文本编辑器里需要保存的html标签都是在这里放行的
		 */
		return  Whitelist.relaxed()
		/**
		 * 必须要删除应用在 a 与 img 上的 protocols，否则就只有使用了这些 protocol 的才不被过滤，比较蛋疼
		 * 在 remove 的时候，后面的 protocols 要完全一个不露的对应上 jsoup 默认已经添加的，否则仍然会被过滤掉
		 * 在升级 jsoup 后需要测试这 a 与 img 的过滤是否正常
		 */
		.removeProtocols("a", "href", "ftp", "http", "https", "mailto")
		.removeProtocols("img", "src", "http", "https")
		.addAttributes("a", "href", "title", "target")  // 官方默认会将 target 给过滤掉
		/**
		 * 放开 video
		 */
		.addAttributes("video", "src", "controls", "width", "height", "poster", "preload", "muted", "loop", "autoplay")
		.addAttributes("source", "src", "type")
		/**
		 * 在 Whitelist.relaxed() 之外添加额外的白名单规则
         */
		.addTags("div", "span", "embed", "object", "param", "font", "s", "em")
		.addAttributes(":all", "style", "class", "id", "name", "size")
		.addAttributes("object", "width", "height", "classid", "codebase")
		.addAttributes("param", "name", "value")
		.addAttributes("font", "color")
		.addAttributes("embed", "src", "quality", "width", "height", "allowFullScreen", "allowScriptAccess", "flashvars", "name", "type", "pluginspage");
	}
	
	/**
	 * 比较通用的过滤方法
	 * <br>过滤Model中的title、name、nickName、userName、enName、remark
	 * 
	 * @param m
	 */
	@SuppressWarnings("rawtypes")
	public static void filterModel(Model m) {
		String title = m.getStr("title");
		if (title != null) {
			m.set("title", getText(title));
		}
		String name = m.getStr("name");
		if (title != null) {
			m.set("name", getText(name));
		}
		String nickName = m.getStr("nickName");
		if (title != null) {
			m.set("nickName", getText(nickName));
		}
		String userName = m.getStr("userName");
		if (title != null) {
			m.set("userName", getText(userName));
		}
		String enName = m.getStr("enName");
		if (title != null) {
			m.set("enName", getText(enName));
		}
		String remark = m.getStr("remark");
		if (title != null) {
			m.set("remark", getText(remark));
		}
	}
	
	/**
	 * 获取 html 中的纯文本信息，过滤所有 tag
 	 */
	public static String getText(String html) {
		return html != null ? Jsoup.clean(html, Whitelist.none()) : null;
	}
	
	/**
	 * 使用Whitelist.simpleText() 白名单，获取 simple html 内容
	 * 允许的 tag："b", "em", "i", "strong", "u"
	 */
	public static String getSimpleHtml(String html) {
		return html != null ? Jsoup.clean(html, Whitelist.simpleText()) : null;
	}

	/**
	 * 使用Whitelist.simpleText() 白名单，获取的 basic 内容
	 * 允许的 tag："a", "b", "blockquote", "br", "cite", "code", "dd", "dl", "dt", "em",
	 *                     "i", "li", "ol", "p", "pre", "q", "small", "span", "strike", "strong", "sub",
	 *                     "sup", "u", "ul"
	 */
	public static String getBasic(String html) {
		checkXssAndCsrf(html);
		
		return html != null ? Jsoup.clean(html, Whitelist.basic()) : null;
	}
	
	/**
	 * 使用Whitelist.basicWithImages() 白名单，获取的 basic with images 内容
	 */
	public static String getBasicWithImages(String html) {
		checkXssAndCsrf(html);
		
		return html != null ? Jsoup.clean(html, Whitelist.basicWithImages()) : null;
	}
	
	public static void checkXssAndCsrf(String html) {
		Document doc = Jsoup.parseBodyFragment(html);
		if (linkHrefContainsJavascript(doc)) {
			throw new RuntimeException("链接包含 javascript 脚本，html 内容为: " + html);
		}
		
		if (srcPointToAction(doc)) {
			throw new RuntimeException("src 未指向静态资源，html 内容为: " + html);
		}
	}
	
	/**
	 * 防止 img 等标签的 src 属性指向 action 进行 csrf 攻击
	 */
	public static boolean srcPointToAction(Document doc) {
		Elements elements = doc.select("[src]");
		for (Element e : elements) {
			String src = e.attr("src");
			if (src != null) {
				int questionCharIndex = src.indexOf('?');
				if (questionCharIndex != -1) {
					src = src.substring(0, questionCharIndex);
				}
				
				if (src.indexOf('.') == -1) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	/**
	 * 防止 a 标签的 href 属性中注入 javascript 脚本，例如：
	 * <a href="javascript:alert(0);">点我</a>
	 */
	public static boolean linkHrefContainsJavascript(Document doc) {
		Elements elements = doc.select("a[href]");
		for (Element e : elements) {
			String href = e.attr("href");
			if (href != null && href.toLowerCase().indexOf("javascript") != -1) {
				return true;
			}
		}
		
		return false;
	}

	/**
	 * 使用Whitelist.relaxed() 白名单，获取比较宽松的内容
	 * 允许的 tag: "a", "b", "blockquote", "br", "caption", "cite", "code", "col",
	 *                   "colgroup", "dd", "div", "dl", "dt", "em", "h1", "h2", "h3", "h4", "h5", "h6",
	 *                   "i", "img", "li", "ol", "p", "pre", "q", "small", "span", "strike", "strong",
	 *                   "sub", "sup", "table", "tbody", "td", "tfoot", "th", "thead", "tr", "u", "ul"
	 */
	public static String getRelaxed(String html) {
		return html != null ? Jsoup.clean(html, Whitelist.relaxed()) : null;
	}
	
	/**
	 * 使用指定的 Whitelist 进行过滤
	 */
	public static String getWithWhitelist(String html, Whitelist whitelist) {
		return html != null ? Jsoup.clean(html, whitelist) : null;
	}

	/**
	 * 使用指定的 tags 进行过滤
	 */
	public static String getWithTags(String html, String... tags) {
		return html != null ? Jsoup.clean(html, Whitelist.none().addTags(tags)) : null;
	}
	
	/**
	 * 专门过滤article
	 * 
	 * @param article
	 */
	public static void filterArticle(Article article){
		String title = article.getTitle();
		if(title != null){
			article.setTitle(getText(title));
		}
		String subTitle = article.getSubtitle();
		if(subTitle != null){
			article.setSubtitle(getText(subTitle));
		}
		String contentText = article.getContentText();
		if(contentText != null){
			article.setContentText(getText(contentText));
		}
		String content = article.getContent();
		if(content != null){
			article.setContent(filterArticleContent(content));
		}
	}
	
	/**
	 * 对文章 content 字段过滤
	 */
	public static String filterArticleContent(String content) {
		checkXssAndCsrf(content);
		
		// return content != null ? Jsoup.clean(content, contentWhitelist) : null;
		// 添加 notPrettyPrint 参数，避免重新格式化，主要是 at me 时候不会在超链前面添加 "\n"
		return content != null ? Jsoup.clean(content, "", contentWhitelist, notPrettyPrint) : null;
	}
	
	/**
	 * 专门过滤site
	 * 
	 * @param site
	 */
	public static void filterSite(Site site){
		String siteName = site.getSiteName();
		if(siteName != null){
			site.setSiteName(getText(siteName));
		}
		String siteSign = site.getSiteSign();
		if(siteSign != null){
			site.setSiteSign(getText(siteSign));
		}
		String siteDes = site.getSiteDes();
		if(siteDes != null){
			site.setSiteDes(getText(siteDes));
		}
	}
}
