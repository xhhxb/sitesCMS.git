package com.sites.common.kit;

import com.jfinal.kit.PathKit;
import com.jfinal.kit.StrKit;

/**
 * 通用工具类
 */
public class SitesKit {

    private static Boolean deployMode = null;

    /**
     * 判断是否是部署模式，用于加载配置文件使用 <br>
     * 判断依据是classPath目录中是否包含eclipse、workspaces、temp、test、target等字样，如果是则不是部署模式
     *
     *
     * @return
     * @author zhangyg3 2020年6月11日 下午2:39:26
     */
    public static boolean isDeployMode() {
        if (deployMode == null) {
            deployMode = buildDeployMode();
        }

        return deployMode;
    }

    /**
     * 构建部署模式的值
     *
     * @return
     * @author zhangyg3 2020年6月11日 下午2:41:12
     */
    private static boolean buildDeployMode() {
        String rootClassPath = PathKit.getRootClassPath();
        if (StrKit.notBlank(rootClassPath)) {
            if (rootClassPath.contains("workspaces")
                    || rootClassPath.contains("workspace")
                    || rootClassPath.contains("eclipse")
                    || rootClassPath.contains("temp")
                    || rootClassPath.contains("test")
                    || rootClassPath.contains("target")
                    || rootClassPath.contains("idea")) {
                return false;
            }
        }

        return true;
    }

}
