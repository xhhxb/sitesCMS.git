package com.sites.common;

import java.io.File;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.kit.PathKit;
import com.jfinal.render.RenderManager;
import com.jfinal.template.Template;

/**
 * 网页静态化转换拦截器
 * 目前只对首页进行静态页生成
 * 如有需要其他页面可参考该逻辑进行生成
 * 
 * @author zyg 2021年7月23日 下午11:14:28
 */
public class RewriteInterceptor implements Interceptor {

	@Override
	public void intercept(Invocation inv) {
		
		if(SiteInfo.useShtml) {//使用静态页的情况
			// 获取当前请求的方法名
			String methodName = inv.getMethodName();
			// 获取静态页面的路径
			String shtmlPath = PathKit.getWebRootPath() + File.separator + "cds" + File.separator + SiteInfo.siteSign + File.separator;
			File shtmlFile = new File(shtmlPath, "index.shtml");
			
			//请求的文章更新方法
			if ("saveArt".equals(methodName) || "updateArt".equals(methodName) || "deleteArt".equals(methodName)
					|| "publishArt".equals(methodName) || "savePicArt".equals(methodName)) {
				inv.invoke();
				//静态页失效，删除
				if(shtmlFile.exists()) {
					shtmlFile.delete();
				}
			} else if("index".equals(methodName)) {
				/*
				 * 请求的是首页
				 */
				Controller controller = inv.getController();
				// 获取首页的Template对象
				Template template = RenderManager.me().getEngine().getTemplate("/cds/" + SiteInfo.siteSign + "/index.html");
				// 当前使用静态页并且静态页面存在，直接渲染静态页面
				if(shtmlFile.exists()) {
					controller.render("index.shtml");
				} else {
					//静态页不存在时先放行请求，再自动生成一个静态页，便于下次使用
					inv.invoke();
					template.render(null, shtmlFile);
				}
			} else {
				//直接放行
				inv.invoke();
			}
		} else {//不使用静态页的情况
			//直接放行
			inv.invoke();
		}
	}
}
