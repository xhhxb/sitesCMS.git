package com.sites.common;

import com.jfinal.server.undertow.UndertowServer;

/**
 * 工程启动类
 * 启动脚本jfinal.bat/jfinal.sh中的MAIN_CLASS要指向这里
 * 
 * @author zyg
 * 2020年8月1日 下午9:32:06
 */
public class RunApp {

	public static void main(String[] args) {
		//没有指定端口，使用undertow.txt中配置端口
		UndertowServer.start(CmsConfig.class);
	}
	
}
