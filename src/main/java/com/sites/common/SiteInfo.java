package com.sites.common;

/**
 * 站点信息
 * 所有的站点信息都静态变量存储一份
 * 
 * @author zyg
 * 2020年1月2日 下午10:19:28
 */
public class SiteInfo {

	/**
	 * 站点信息
	 * 只有变量名，变量值是在系统启动后根据当前主站点自动赋值的 
	 */
	public static int siteId;
	public static String siteName;
	public static String siteSign;
	public static String siteUrl;//网站域名，CSRF防护时也会用到
	public static String uploadPath;
	public static String siteTitle;//SEO标题
	public static String siteKeys;//SEO关键字
	public static String siteDes;//SEO描述
	public static String icpNo;//ICP备案号
	public static String securityNo;//公安备案号
	public static int imgMaxSize;//上传图片的最大限定，单位M
	public static int videoMaxSize;//上传视频的最大限定，单位M
	public static int fileMaxSize;//上传文件最大限定，单位M
	public static String fileSuffix;//附件支持类型，用于layui上传附件
	public static String thumbnailSuffix;//缩略图支持的文件类型，用于layui上传图片
	public static String editorImgSuffix;//富文本上传图片支持的类型，与缩略图类型一致只是格式不同
	public static String editorVideoSuffix;//富文本上传视频支持类型
	public static String videoSuffix;//上传视频支持类型的另一种格式
	public static String cdsAccessLog;
	public static String cmsAccessLog;
	
	/**
	 * 是否固定站点的标示
	 */
	public static boolean fixedSite;
	
	/**
	 * 
	 */
	public static String downloadFileFromUrl;
	
	/**
	 * 公司信息
	 */
	public static String company_id;//记录id
	public static String company_name;//公司名称
	public static String company_address;//公司地址
	public static String company_postCode;//邮政编码
	public static String company_contacts;//联系人
	public static String company_mobile;//手机号
	public static String company_telephone;//座机
	public static String company_fax;//传真
	public static String company_qq;//qq
	public static String company_email;//邮箱
	public static Integer company_logo;//logo图片id
	public static Integer company_weChatImg;//微信图片id
	public static String company_license;//营业执照
	public static String company_website;//网站地址
	public static String company_remark;//其他信息
	
	/**
	 * 程序版本号<br/>
	 * 版本号分为3个部分，对应信息如下：<br/>
	 * 第一部分：大版本号，整体架构优化、调整该部分增加<br/>
	 * 第二部分：中版本号，新增功能、架构优化等较大内容调整该部分增加<br/>
	 * 第三部分：小版本号，bug修复、功能优化等较小内容调整该部分增加
	 */
	public static String version = "3.2.0";
	/**
	 * 版本更新时间<br/>
	 * 每一次更新版本号都同步修改更新时间
	 */
	public static String updateDate = "2023-03-16";
	
	/**
	 * 前后端交互msg的key
	 */
	public static String msgKey = "msg";
	/**
	 * 表单提交使用的验证token的key
	 */
	public static String formTokenKey = "csrf_form_token";
	/**
	 * 前后端交互code的key
	 */
	public static String codeKey = "code";
	/**
	 * 前后端交互token的key，api模块专用
	 */
	public static String tokenKey = "Token";
	/**
	 * 在session中存放登录用户的key
	 */
	public static String account = "account";
	/**
	 * 正常状态的字符串
	 */
	public static String statusNormal = "1";
	/**
	 * 删除状态的字符
	 */
	public static String statusDel = "2";
	/**
	 * 用户权限缓存名
	 */
	public static String accPermCacheName = "accountPermCacheName";
	/**
	 * 栏目缓存名
	 */
	public static String columnsCacheName = "columnsCacheName";
	/**
	 * 权限表缓存名
	 */
	public static String permCacheName = "permCacheName";
	/**
	 * 服务响应异常信息
	 */
	public static String serverError = "服务响应异常，请联系系统管理员！";
	/**
	 * 超级管理员的用户名，该用户不受权限控制
	 */
	public static String adminName = "wumoxi";
	/**
	 * 日志类型-cds
	 */
	public static String cdsLog = "cds";
	/**
	 * 日志类型-cms
	 */
	public static String cmsLog = "cms";
	/**
	 * 参数值长度的最大长度，当超过该超度时截取后存储，用于访问信息记录功能
	 */
	public static int paraValuesMaxLength = 100;
	/**
	 * 是否适用shtml静态页的标记字段
	 */
	public static boolean useShtml = false;

	/**
	 * 文本编辑器，默认是wangEditor
	 */
	public static String editor = "wangEditor";
	/**
	 * 新增文章页面名
	 */
	public static String pageArtAdd = "pageArtAdd.html";
	/**
	 * 编辑文章页面名
	 */
	public static String pageArtEdit = "pageArtEdit.html";

}
