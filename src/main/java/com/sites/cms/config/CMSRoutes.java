package com.sites.cms.config;

import com.jfinal.config.Routes;
import com.sites.cms.accessLog.AccessLogController;
import com.sites.cms.account.AccountController;
import com.sites.cms.article.ArticleController;
import com.sites.cms.column.ColumnController;
import com.sites.cms.common.CmsAccessInterceptor;
import com.sites.cms.common.GlobalErrorInterceptor;
import com.sites.cms.common.ReturnInterceptor;
import com.sites.cms.common.pjax.PjaxInterceptor;
import com.sites.cms.company.CompanyController;
import com.sites.cms.file.FileController;
import com.sites.cms.index.IndexController;
import com.sites.cms.login.LoggedInterceptor;
import com.sites.cms.login.LoginController;
import com.sites.cms.permission.PermissionController;
import com.sites.cms.permission.PermissionInterceptor;
import com.sites.cms.role.RoleController;
import com.sites.cms.site.SiteController;
import com.sites.common.safe.CSRFInterceptor;

/**
 * cms路由配置
 * 
 * @author zyg
 * 2020年1月4日 下午1:15:19
 */
public class CMSRoutes extends Routes {

	@Override
	public void config() {
		/*
		 * 配置视图基础路径，下面Controller返回的页面都是在这个目录下的
		 */
		setBaseViewPath("/cms");

		/*
		 * 配置拦截器，对下面所有的Controller都生效
		 */
		addInterceptor(new GlobalErrorInterceptor());//全局异常处理拦截器
		addInterceptor(new PjaxInterceptor());//pjax处理拦截器
		addInterceptor(new LoggedInterceptor());//登录限定拦截器
		addInterceptor(new PermissionInterceptor());//权限拦截器
		addInterceptor(new CmsAccessInterceptor());//访问信息拦截器
		addInterceptor(new CSRFInterceptor());//跨站攻击防御拦截器
		addInterceptor(new ReturnInterceptor());

		/*
		 * 配置请求
		 */
		add("/cmsLogin", LoginController.class, "login");
		add("/cms", IndexController.class, "index");
		add("/cmsAccount", AccountController.class, "account");
		add("/cmsRole", RoleController.class, "role");
		add("/cmsPerm", PermissionController.class, "permission");
		add("/cmsSite", SiteController.class, "site");
		add("/cmsColumn", ColumnController.class, "column");
		add("/cmsArt", ArticleController.class, "article");
		add("/cmsLog", AccessLogController.class, "accessLog");
		add("/cmsCompany", CompanyController.class, "company");
		add("/file", FileController.class);
	}

}
