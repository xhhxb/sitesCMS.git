package com.sites.cms.role;

import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;
import com.jfinal.validate.Validator;
import com.sites.common.SiteInfo;

/**
 * 角色校验器
 * 
 * @author zyg
 * 2020年11月8日 下午1:29:49
 */
public class RoleValidator extends Validator {

	@Override
	protected void validate(Controller c) {
		setShortCircuit(true);
		validateRequired("name", SiteInfo.msgKey, "角色名称不可为空！");
		validateString("name", 1, 100, SiteInfo.msgKey, "角色名称最长为100个字符！");
		validateString("remark", 0, 500, SiteInfo.msgKey, "备注最长为500个字符！");
	}

	@Override
	protected void handleError(Controller c) {
		Ret ret = Ret.fail(SiteInfo.msgKey, c.getAttr(SiteInfo.msgKey));
		ret.set(SiteInfo.formTokenKey, c.createToken(SiteInfo.formTokenKey, 1800));//将更新后的token返回前端，避免token校验失败
		c.renderJson(ret);
	}

}
