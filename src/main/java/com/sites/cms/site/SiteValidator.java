package com.sites.cms.site;

import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;
import com.jfinal.validate.Validator;
import com.sites.common.SiteInfo;

/**
 * 站点校验器
 * 
 * @author zyg
 * 2020年11月8日 下午2:17:24
 */
public class SiteValidator extends Validator {

	@Override
	protected void validate(Controller c) {
		setShortCircuit(true);
		validateRequired("siteName", SiteInfo.msgKey, "站点名称不可为空！");
		validateString("siteName", 1, 100, SiteInfo.msgKey, "站点名称最大长度为100个字符！");
		
		validateRequired("siteSign", SiteInfo.msgKey, "站点标识不可为空！");
		validateString("siteSign", 1, 100, SiteInfo.msgKey, "站点标识最大长度为100个字符！");
		validateRegex("siteSign", "[0-9A-Za-z]+", SiteInfo.msgKey, "站点标识只能是英文字母或数字！");
		
		validateRequired("imgMaxSize", SiteInfo.msgKey, "图片大小不可为空！");
		validateInteger("imgMaxSize", SiteInfo.msgKey, "图片大小必须输入整数！");
		
		validateRequired("fileMaxSize", SiteInfo.msgKey, "文件大小不可为空！");
		validateInteger("fileMaxSize", SiteInfo.msgKey, "文件大小必须输入整数！");
		
		validateRequired("videoMaxSize", SiteInfo.msgKey, "视频大小不可为空！");
		validateInteger("videoMaxSize", SiteInfo.msgKey, "视频大小必须输入整数！");
		
		validateRequired("imgSuffix", SiteInfo.msgKey, "图片格式不可为空！");
		validateRequired("fileSuffix", SiteInfo.msgKey, "文件格式不可为空！");
		validateRequired("videoSuffix", SiteInfo.msgKey, "视频格式不可为空！");
	}

	@Override
	protected void handleError(Controller c) {
		Ret ret = Ret.fail(SiteInfo.msgKey, c.getAttr(SiteInfo.msgKey));
		ret.set(SiteInfo.formTokenKey, c.createToken(SiteInfo.formTokenKey, 1800));//将更新后的token返回前端，避免token校验失败
		c.renderJson(ret);
	}

}
