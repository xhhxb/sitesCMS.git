package com.sites.cms.site;

import java.util.List;

import com.jfinal.aop.Before;
import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;
import com.sites.cms.permission.Remark;
import com.sites.common.SiteInfo;
import com.sites.common.model.Site;

/**
 * 站点管理控制器
 * 
 * @author zyg
 * 2020年2月1日 下午1:24:00
 */
public class SiteController extends Controller {
	
	@Inject
	private SiteService srv;

	@Remark("菜单：新增站点")
	public void enterSiteAdd(){
		render("pageSiteAdd.html");
	}
	
	@Remark("按钮：保存站点")
	@Before(SiteValidator.class)
	public void saveSite(){
		Site site = getBean(Site.class, "");
		Ret ret = srv.saveSite(site);
		renderJson(ret);
	}
	
	@Remark("菜单：站点管理")
	public void enterSiteMng(){
		List<Site> siteList = srv.getAllSites();
		setAttr("siteList", siteList);
		
		render("pageSiteMng.html");
	}
	
	@Remark("按钮：编辑站点")
	public void enterSiteEdit(){
		int id = getParaToInt(0);
		Site site = srv.getById(id);
		setAttr("site", site);
		render("pageSiteEdit.html");
	}
	
	@Remark("按钮：更新站点")
	@Before(SiteValidator.class)
	public void updateSite(){
		Site site = getBean(Site.class, "");
		Ret ret = srv.updateSite(site);
		renderJson(ret);
	}
	
	@Remark("按钮：设置当前站点")
	public void setMainSite(){
		int id = getParaToInt(0);
		Ret ret = srv.setMainSite(id);
		renderJson(ret);
	}
	
	/**
	 * 进入当前站点信息修改页面
	 */
	@Remark("菜单：系统设置")
	public void enterCurrentSiteEdit(){
		//当前站点信息不再查询数据库，直接使用SiteInfo中的值即可
		render("pageCurrentSiteEdit.html");
	}
	
	@Remark("按钮：更新系统设置信息")
	public void updateCurrentSite(){
		Site site = getBean(Site.class, "");
		Ret ret = srv.updateCurrentSite(site);
		renderJson(ret);
	}
	
	@Remark("按钮：恢复默认站点")
	public void resetSite(){
		Ret ret = srv.resetSite();
		renderJson(ret);
	}

	@Remark("按钮：删除站点")
	public void deleteSite(){
		int id = getParaToInt(0);
		Ret ret = srv.deleteSite(id);
		renderJson(ret);
	}
}
