package com.sites.cms.company;

import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;
import com.sites.cms.permission.Remark;
import com.sites.common.SiteInfo;
import com.sites.cms.file.FileService;
import com.sites.common.model.Company;
import com.sites.common.model.Files;

/**
 * 公司信息控制器
 * 
 * @author zyg
 * 2021年10月31日 下午10:55:26
 */
public class CompanyController extends Controller {

	@Inject
	private CompanyService srv;
	@Inject
	private FileService fileSrv;
	
	@Remark("菜单：进入公司信息管理页面")
	public void enterCompanyMng() {
		Company company = srv.getCompany();
		setAttr("company", company);
		if(company!=null && company.getLogo()!=null) {
			Files logo = fileSrv.getById(company.getLogo());
			setAttr("logo", logo);
		}
		if(company!=null && company.getWeChatImg()!=null) {
			Files WeChatImg = fileSrv.getById(company.getWeChatImg());
			setAttr("WeChatImg", WeChatImg);
		}
		
		render("pageCompanyMng.html");
	}
	
	@Remark("按钮：更新公司信息")
	public void updateCompany() {
		Company company = getBean(Company.class, "");
		Ret ret = srv.updateCompany(company);
		renderJson(ret);
	}
	
}
