package com.sites.cms.permission;

import java.util.List;

import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;
import com.sites.common.SiteInfo;
import com.sites.common.model.Permission;

/**
 * 权限控制器
 * 
 * @author zyg
 * 2020年1月29日 上午10:54:29
 */
public class PermissionController extends Controller {

	@Inject
	private PermissionService srv;
	
	@Remark("菜单：权限管理")
	public void enterPermMng(){
		List<Permission> permissionList = srv.getAllPermission();
		setAttr("permissionList", permissionList);
		render("pagePermMng.html");
	}
	
	/**
	 * 重新加载权限
	 */
	@Remark("按钮：一键同步权限列表")
	public void reloadPerm(){
		Ret ret = srv.reloadPerm();
		renderJson(ret);
	}
	
	@Remark("按钮：编辑权限信息")
	public void enterPermEdit(){
		int id = getParaToInt(0);
		Permission permission = srv.getById(id);
		setAttr("perm", permission);
		render("pagePermEdit.html");
	}
	
	@Remark("按钮：更新权限信息")
	public void updatePerm(){
		Permission perm = getBean(Permission.class, "");
		Ret ret = srv.updatePerm(perm);
		renderJson(ret);
	}
	
	@Remark("按钮：删除权限信息")
	public void deletePerm(){
		int id = getInt(0);
		Ret ret = srv.deletePerm(id);
		renderJson(ret);
	}
}
