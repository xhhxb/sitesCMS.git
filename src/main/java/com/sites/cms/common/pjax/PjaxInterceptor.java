package com.sites.cms.common.pjax;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;

/**
 * pjax拦截器
 * <br/>和前端页面配合进行局部或者全部页面渲染
 * 
 * @author zhangyg3
 * 2019年1月31日  下午8:42:14
 */
public class PjaxInterceptor implements Interceptor {

	public void intercept(Invocation inv) {
		try {
			inv.invoke();
		} finally {
			Controller c = inv.getController();
			boolean isPjax = "true".equalsIgnoreCase(c.getHeader("X-PJAX"));
			c.setAttr("isPjax", isPjax);//页面根据这个参数来判断进行局部页面刷新还是全部页面渲染
		}
	}

}
