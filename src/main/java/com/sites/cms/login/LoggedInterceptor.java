package com.sites.cms.login;

import javax.servlet.http.HttpSession;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;
import com.sites.common.SiteInfo;
import com.sites.common.model.Account;

/**
 * 已登录拦截器
 * <br/>用于限定用户必须登录才能进行的操作
 * 
 * @author zyg
 * 2020年1月31日 上午11:19:03
 */
public class LoggedInterceptor implements Interceptor {

	public void intercept(Invocation inv) {
		Controller controller = inv.getController();
		HttpSession session = controller.getSession();
		Account account = (Account) session.getAttribute(SiteInfo.account);
		if(account == null){
			String methodName = inv.getMethodName();
			//根据不同的请求方法进行提示处理，依托于sitesCMS设计规范
			if(methodName.startsWith("enter") || "index".equals(methodName)){
				controller.redirect("/cmsLogin");
			} else {
				Ret ret = Ret.fail(SiteInfo.msgKey, "登录超时，请重新登录！");
				controller.renderJson(ret);
			}
		} else {
			inv.invoke();
		}
		
	}

}
