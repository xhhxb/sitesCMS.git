#sql("queryArtsPage")
	SELECT
		a.*, f.path thbPath
	FROM
		article a
	LEFT JOIN files f ON a.thumbnail = f.id
	WHERE
		a.siteId = #para(siteId)
	AND a.`column` = #para(column)
	AND a.status = '1'
	ORDER BY
	    a.isTop DESC,
		a.updateTime DESC,
		a.createTime DESC
#end

#sql("getColumnByEnName")
	select * from `column` t where t.enName = ? and t.siteId = ?
#end

#sql("getArtById")
	SELECT
		a.*, f.path thbPath
	FROM
		article a
	LEFT JOIN files f ON a.thumbnail = f.id
	WHERE
		a.id = ?
#end

#sql("getColLastArt")
	SELECT
		a.*, f.path thbPath
	FROM
		article a
	LEFT JOIN files f ON a.thumbnail = f.id
	WHERE
		a.column = ?
	ORDER BY 
		a.updateTime DESC,
		a.createTime DESC
	limit 1
#end