#sql("getAllPermission")
	select * from permission t where t.siteId = ? order by t.controller, t.status, t.remark desc
#end

#sql("getNormalPerms")
	select * from permission t where t.status = '1' and siteId = ? order by t.controller, t.status, t.remark desc
#end

#sql("getByAction")
	select * from permission where actionKey=? and controller = ? and siteId = ? limit 1
#end

#sql("getByActionKey")
	select * from permission where actionKey=? and siteId = ? limit 1
#end

#sql("getAccountPerm")
	SELECT
		p.*
	FROM
		permission p
	LEFT JOIN rolepermission rp ON p.id = rp.permissionId
	LEFT JOIN accountrole ar ON rp.roleId = ar.roleId
	LEFT JOIN account a ON ar.accountId = a.id
	WHERE
		a.id = ?
	AND p.siteId = ?
 order by p.controller, p.status, p.remark desc
#end

#sql("delSitePerm")
    delete from permission where siteId = ?
#end