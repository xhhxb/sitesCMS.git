#sql("getByName")
	select * from account t where t.userName = ? and t.siteId = ? limit 1
#end

#sql("queryAccountsPage")
	SELECT
		*
	FROM
		account t
	WHERE
		t.siteId = #para(siteId)
	#if(status && status>-1)
		AND t.status = #para(status)
	#end
	#if(nickName)
		AND t.nickName like #para(nickName, "like")
	#end
	#if(userName)
		AND t.userName like #para(userName, "like")
	#end
	ORDER BY 
	t.createTime DESC
#end

#sql("getAccountRoles")
	SELECT
		*
	FROM
		role t
	WHERE
		t.id IN (
			SELECT
				a.roleId
			FROM
				accountrole a
			WHERE
				a.accountId = ?
		)
#end

#sql("addRole")
	insert into accountrole(accountId, roleId) values(?, ?)
#end

#sql("delRole")
	delete from accountrole where accountId = ? and roleId = ?
#end

#sql("getAdmin")
	select * from account t where t.userName = ? and t.siteId = '0' limit 1
#end

#sql("queryAllAccounts")
	select * from account t where t.siteId = #para(siteId)
#end

#sql("deleteSiteAccountRole")
    delete from accountrole where accountId in (select id from account where siteid = ?)
#end

#sql("deleteSiteAccount")
    delete from account where siteid = ?
#end