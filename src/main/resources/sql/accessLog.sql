#sql("getAccessLogs")
	select * from accesslog t where 1=1
	#if(type)
		and t.type = #para(type)
	#end
	#if(actionKey)
		and t.actionKey = #para(actionKey)
	#end
	#if(siteId)
		and t.siteId = #para(siteId)
	#end
#end

#sql("getAccessLogsTrend")
    select date_format(t.accessTime, '%m-%d') days, count(1) daysNum
     from accesslog t
    where 1 = 1
        #if(type)
            and t.type = #para(type)
        #end
        #if(actionKey)
		    and t.actionKey = #para(actionKey)
	    #end
	    #if(actionKeyLike)
		    and t.actionKey like #para(actionKeyLike)
	    #end
        #if(siteId)
		    and t.siteId = #para(siteId)
	    #end
        and t.accessTime >= date_sub(curdate(), interval 7 day)
    group by days
    order by days
#end

#sql("queryCdsLogPage")
	SELECT
		*
	FROM
		accesslog t
	WHERE
		1 = 1
	#if(type)
		AND t.type = #para(type)
	#end
	#if(actionKey)
		AND t.actionKey = #para(actionKey)
	#end
	#if(ip)
		AND t.ip = #para(ip)
	#end
	#if(startDate)
		AND t.accessTime >= STR_TO_DATE(#para(startDate), '%Y-%m-%d')
	#end
	#if(endDate)
		AND t.accessTime < STR_TO_DATE(#para(endDate), '%Y-%m-%d')
	#end
	#if(siteId)
		and t.siteId = #para(siteId)
	#end
		ORDER BY
			t.accessTime DESC
#end

#sql("queryCmsLogPage")
	SELECT
		t.type,
		t.ip,
		a.nickName 'visitor',
		p.remark 'actionKey',
		t.parameter,
		t.accessTime,
		t.remark
	FROM
		accesslog t
	LEFT JOIN permission p ON t.actionKey = p.actionKey
	AND p.siteId = #para(siteId)
	LEFT JOIN account a ON t.visitor = a.userName
	AND a.siteId = #para(siteId)
	WHERE
		1 = 1
	#if(type)
		AND t.type = #para(type)
	#end
	#if(ip)
		AND t.ip = #para(ip)
	#end
	#if(visitor)
		AND t.visitor = #para(visitor)
	#end
	#if(actionKey)
		AND t.actionKey = #para(actionKey)
	#end
	#if(startDate)
		AND t.accessTime >= STR_TO_DATE(#para(startDate), '%Y-%m-%d')
	#end
	#if(endDate)
		AND t.accessTime < STR_TO_DATE(#para(endDate), '%Y-%m-%d')
	#end
	#if(siteId)
		and t.siteId = #para(siteId)
	#end
		ORDER BY
			t.accessTime DESC
#end

#sql("deleteLog")
    delete from accesslog t where t.siteId = ?
#end