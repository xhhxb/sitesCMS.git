#sql("getArtListByCol")
	SELECT
		a.*, f.`name` AS thbName,
		f.path AS thbPath
	FROM
		article a
	LEFT JOIN `column` c ON a.`column` = c.id
	LEFT JOIN files f ON a.thumbnail = f.id
	WHERE
		c.enName = ?
	AND c.siteId = ?
	AND a.status = '1'
	ORDER BY
	    a.isTop DESC,
		a.createTime DESC
	LIMIT ?
#end

#sql("getArtListByColPro")
	SELECT
		a.*, f.`name` AS thbName,
		f.path AS thbPath
	FROM
		article a
	LEFT JOIN `column` c ON a.`column` = c.id
	LEFT JOIN files f ON a.thumbnail = f.id
	WHERE
		c.enName = #para(enName)
	AND c.siteId = #para(siteId)
	AND a.status = '1'
	ORDER BY
	    a.isTop DESC,
		#switch (orderType)
		  #case (1)
		    a.createTime DESC
		  #case (2)
		    a.createTime ASC
		  #case (3)
		    a.title ASC
		  #case (4)
		    a.title DESC
		  #default
		    a.createTime DESC
		#end
	LIMIT #para(listSize)
#end

#sql("getColChildrenByCol")
	SELECT
		c.*
	FROM
		`column` c
	LEFT JOIN `column` p ON c.parent = p.id
	WHERE
		p.enName = ?
	AND p.siteId = ?
	AND c.status = '1'
	ORDER BY
		c.order,
		c.createTime DESC
#end

#sql("getArtListByColIds")
	SELECT
		*
	FROM
		article t
	WHERE
		t.siteId = #para(siteId)
		AND t.status = '1'
		#for(x : columnIds)
			#if(for.size == 1)
				AND t.`column` = #para(x)
			#else if(for.first)
				AND ( t.`column` = #para(x)
			#else if(for.last)
				OR t.`column` = #para(x) )
			#else 
				OR t.`column` = #para(x)
			#end
		#end
	ORDER BY
		t.createTime DESC
	LIMIT #para(listSize)
#end

#sql("getArtListByColEnNames")
	SELECT
		a.*, f.`name` AS thbName,
		f.path AS thbPath
	FROM
		article a
	LEFT JOIN `column` c ON a.`column` = c.id
	LEFT JOIN files f ON a.thumbnail = f.id
	WHERE
		a.siteId = #para(siteId)
		AND a.status = '1'
		#for(x : colEnNameList)
			#if(for.size == 1)
				AND c.enName = #para(x)
			#else if(for.first)
				AND ( c.enName = #para(x)
			#else if(for.last)
				OR c.enName = #para(x) )
			#else
				OR c.enName = #para(x)
			#end
		#end
	ORDER BY
	    a.isTop DESC,
		a.createTime DESC
	LIMIT #para(listSize)
#end

#sql("getHotArtListByColEnNames")
	SELECT
		a.*, f.`name` AS thbName,
		f.path AS thbPath
	FROM
		article a
	LEFT JOIN `column` c ON a.`column` = c.id
	LEFT JOIN files f ON a.thumbnail = f.id
	WHERE
		a.siteId = #para(siteId)
		AND a.status = '1'
		#for(x : colEnNameList)
			#if(for.size == 1)
				AND c.enName = #para(x)
			#else if(for.first)
				AND ( c.enName = #para(x)
			#else if(for.last)
				OR c.enName = #para(x) )
			#else
				OR c.enName = #para(x)
			#end
		#end
	ORDER BY
		a.clickNum DESC
	LIMIT #para(listSize)
#end

#sql("getFilesByIds")
	select * from files t
	#if(ids)
		where
		#for(id : ids)
			#if(for.first)
				t.id = #(id.toInt())
			#else
				or t.id = #(id.toInt())
			#end
		#end
	#end
#end

#sql("getArtPrior")
	SELECT
		t.id,
		t.title
	FROM
		article t
	WHERE
		t.`column` = #para(columnId)
	AND t.id < #para(artId)
	ORDER BY
		t.id DESC
	LIMIT 1
#end

#sql("getArtNext")
	SELECT
		t.id,
		t.title
	FROM
		article t
	WHERE
		t.`column` = #para(columnId)
	AND t.id > #para(artId)
	ORDER BY
		t.id
	LIMIT 1
#end