#sql("queryArtsPage")
	SELECT
		a.*, f.path thbPath
	FROM
		article a
	LEFT JOIN files f ON a.thumbnail = f.id
	WHERE
		a.siteId = #para(siteId)
	AND a.`column` = #para(column)
	AND a.status = '1'
	ORDER BY
	    a.isTop DESC,
		a.createTime DESC
#end

#sql("queryArtsPagePro")
SELECT
    a.*, f.path thbPath
FROM
    article a
    LEFT JOIN files f ON a.thumbnail = f.id
    LEFT JOIN `column` c ON a.`column` = c.id
WHERE
    a.siteId = #para(siteId)
    AND a.status = '1'
    AND c.enName in #para(columns, "in")
ORDER BY
    a.isTop DESC,
    a.createTime DESC
#end

#sql("getColumnByEnName")
	select * from `column` t where t.enName = ? and t.siteId = ?
#end

#sql("getArtById")
	SELECT
		a.*, f.path thbPath
	FROM
		article a
	LEFT JOIN files f ON a.thumbnail = f.id
	WHERE
		a.id = ?
#end

#sql("getColLastArt")
	SELECT
		a.*, f.path thbPath
	FROM
		article a
	LEFT JOIN files f ON a.thumbnail = f.id
	WHERE
		a.column = ?
	ORDER BY 
		a.updateTime DESC,
		a.createTime DESC
	limit 1
#end

#sql("getColsByParentId")
    SELECT * from `column` t WHERE t.parent = ? ORDER BY t.`order`
#end

#sql("getArtsByColId")
    SELECT * FROM article a WHERE a.`column` = #para(col) ORDER BY
        #switch (orderType)
            #case (1)
		        a.createTime DESC
		    #case (2)
		        a.createTime ASC
		    #case (3)
		        a.title ASC
		    #case (4)
		        a.title DESC
		    #default
		        a.createTime DESC
		#end
#end