#本地数据库配置
driverClass=com.mysql.cj.jdbc.Driver
jdbcUrl=jdbc:mysql://localhost:3306/sitescmsTest?useSSL=false&zeroDateTimeBehavior=convertToNull
user=root
password=IiP3AFVec+Ri0XBsPmTQ/D/b3a1PGWjspNY9XzQiDEYpUlLp2TRTVNjjUE/M0Lns8qp9Ow4vUNfDqPPqlG/41Q==
publicKey=MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAIvyNGs4tUmW6OX5yLVgqxv7+Eee48bWEXURRuHmck7um83eGaiYfUDVgOOC3xWmy7TLGcjD/fsTH2pnXe4ZLHUCAwEAAQ==

#调试模式
devMode=true
engineDevMode=true

#域名、访问地址，CSRF校验的时候用的到，放在配置文件里更加灵活
siteUrl = localhost

#文件上传路径
uploadPath=D:\\develop\\upload\\

#文件下载地址，当多个站点使用统一后台管理时该配置指向统一后台地址
#案例 downloadFileFromUrl=http://39.99.241.13:8888
downloadFileFromUrl=

#使用静态页
useShtml=false

#固定站点标示，不需要固定时该配置设为空
siteSign =

#api接口请求的有效时间，单位分钟
api.betweenMinute = 5
#api接口token使用的秘钥，需要和前端约定好，尽可能负责些且没有实际意义
api.privateKey = lVHtuCOfgXffjNQt